$: << File.expand_path( '../lib/', __FILE__ )
require 'date'

Gem::Specification.new do |s|
  s.name        = 'testing'
  s.version     = '0.0.1'
  s.date        = '2016-06-06'
  s.summary     = "Test"
  s.description = "A program that tests CI in Gitlab"
  s.authors     = ["me"]
  s.email       = ''
  s.files       = ["lib/testing.rb"]
  s.homepage    =
    'https://gitlab.com/judyj/test'
  #s.metadata = {
  #                'issue_tracker' => ''
  #             }
  s.executables = 'testing'
  #s.license       = 'Apache-2.0'
  #
  # gem dependencies
  #   for the published gem
  s.required_ruby_version = '>=1.8.7'
  
  # for development
  s.add_development_dependency 'rake',        '~> 10'
  s.add_development_dependency 'rspec',       '~> 3'
  
  # simple text description of external requirements (for humans to read)
  s.requirements << 'nah'
  
  # ensure the gem is built out of versioned files
  #s.files = Dir['Rakefile', '{bin,lib,spec}/**/*', 'README*', 'LICENSE*'] & `git ls-files -z .`.split("\0")
  s.files = Dir['Rakefile', '{bin,lib,spec}/**/*', 'README*'] & `git ls-files -z .`.split("\0")

end

